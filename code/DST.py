import numpy as np
import math
import basic_program as bp
import time

# Manual entries
# Enter here the numbers of sources you want to combinate first (First Combination)
import Source_1 as x
import Source_2 as y
# Enter the number of the source that you want to combine with the previously calculated mass function (Second Combination)
import Source_3 as z
# Enter the number of the source that you want to combine with the previously calculated mass function (Third/Last Combination)
import Source_6 as w


# First Combination of sources with Dempster`s combination rule
start = time.time()
# Set values of the new mass function
r = 1 - np.multiply(x.omegak, y.omega) - np.multiply(x.omega, y.omegak)

omega = np.divide(np.multiply(x.omega, y.omega) + x.omega * y.theta + x.theta * y.omega, r)
omega = omega.round(4)

theta = np.divide(x.theta * y.theta * x.a, r)

omegak = np.divide(np.multiply(x.omegak, y.omegak) + x.omegak * y.theta + x.theta * y.omegak, r)
omegak = omegak.round(4)

# Set values of the belief function
Belomega = omega
Belomegak = omegak
Beltheta = omega + omegak + theta

# Set values of the plausibility function
Plomega = omega + theta
Plomegak = omegak + theta
Pltheta = omega + omegak + theta


# Three different decision rules
Dempster = np.zeros(len(Belomega))

# weakest decision rule
for k in range(0, len(Belomega)-1):
    if Plomega[k] > Belomegak[k]:
        Dempster[k] = 100

# average decision rule
for k in range(0, len(Belomega)-1):
    if Belomega[k] > Belomegak[k]:
        Dempster[k] = 220

# strongest decision rule
for k in range(0, len(Belomega)-1):
    if Belomega[k] > Plomegak[k]:
        Dempster[k] = 255


Dempster = Dempster.reshape((len(y.array_Mus), len(y.array_Mus[1])))
# save array as image
Image_M = y.Image.fromarray(Dempster)
Image_M = Image_M.convert("L")  # grayscale image
Image_M.save("../outputs/" + bp.filename + "_DempsterKombi1.jpg")

end = time.time()
print("Duration of first combination:",'{:5.3f}s'.format(end-start))

# Second Combination of sources with Dempster`s combination rule
start = time.time()
# Set values of the new mass function
r = 1 - np.multiply(omegak, z.omega) - np.multiply(omega, z.omegak)

omega = np.divide(np.multiply(omega, z.omega) + omega * z.theta + theta * z.omega, r)
omega = omega.round(4)

theta = np.divide(theta * z.theta * z.a, r)

omegak = np.divide(np.multiply(omegak, z.omegak) + omegak * z.theta + theta * z.omegak, r)
omegak = omegak.round(4)

# Set values of the belief function
Belomega = omega
Belomegak = omegak
Beltheta = omega + omegak + theta

# Set values of the plausibility function
Plomega = omega + theta
Plomegak = omegak + theta
Pltheta = omega + omegak + theta


# Three different decision rules
Dempster = np.zeros(len(Belomega))

# weakest decision rule
for k in range(0, len(Belomega)-1):
    if Plomega[k] > Belomegak[k]:
        Dempster[k] = 100

# average decision rule
for k in range(0, len(Belomega)-1):
    if Belomega[k] > Belomegak[k]:
        Dempster[k] = 220

# strongest decision rule
for k in range(0, len(Belomega)-1):
    if Belomega[k] > Plomegak[k]:
        Dempster[k] = 255


Dempster = Dempster.reshape((len(y.array_Mus), len(y.array_Mus[1])))
# save array as image
Image_M = y.Image.fromarray(Dempster)
Image_M = Image_M.convert("L")  # grayscale image
Image_M.save("../outputs/" + bp.filename + "_DempsterKombi2.jpg")

end = time.time()
print("Duration of second combination:",'{:5.3f}s'.format(end-start))

# Third Combination of sources with Dempster`s combination rule
start = time.time()
# Set values of the new mass function
r = 1 - np.multiply(omegak, w.omega) - np.multiply(omega, w.omegak)

omega = np.divide(np.multiply(omega, w.omega) + omega * w.theta + theta * w.omega, r)
omega = omega.round(4)

theta = np.divide(theta * w.theta * w.a, r)

omegak = np.divide(np.multiply(omegak, w.omegak) + omegak * w.theta + theta * w.omegak, r)
omegak = omegak.round(4)

# Set values of the belief function
Belomega = omega
Belomegak = omegak
Beltheta = omega + omegak + theta

# Set values of the plausibility function
Plomega = omega + theta
Plomegak = omegak + theta
Pltheta = omega + omegak + theta


# Three different decision rules
Dempster = np.zeros(len(Belomega))

# weakest decision rule
for k in range(0, len(Belomega)-1):
    if Plomega[k] > Belomegak[k]:
        Dempster[k] = 100

# average decision rule
for k in range(0, len(Belomega)-1):
    if Belomega[k] > Belomegak[k]:
        Dempster[k] = 220

# strongest decision rule
for k in range(0, len(Belomega)-1):
    if Belomega[k] > Plomegak[k]:
        Dempster[k] = 255

Dempster = Dempster.reshape((len(bp.array_r), len(bp.array_r[1])))
# save array as image
Image_M = y.Image.fromarray(Dempster)
Image_M = Image_M.convert("L")  # grayscale image
Image_M.save("../outputs/" + bp.filename + "_DempsterKombi3.jpg")

end = time.time()
print("Duration of third combination:",'{:5.3f}s'.format(end-start))