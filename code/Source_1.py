import basic_program as bp
import numpy as np
import numpy.linalg as linalg
import time
import matplotlib.pyplot as plt
from PIL import Image
start = time.time()


# Source 1: Segmentation using the RGB values of a pixel using the Euclidean distance in R^3
print("Source 1:")

# No manual entries

# Unbiased sample mean of the training set (3-dim)
mt = np.array((0, 0, 0))
for i in range(0, len(bp.array_Tr)):
    mt = mt + bp.RGBvaluesTrainingset(i)

mt = mt / len(bp.array_Tr)
print("Unbiased sample mean of the training set:")
print(mt)

# Euclidean distance unscaled
p = 10000000000
q = 0
array_Eus = np.zeros((len(bp.array_r), len(bp.array_r[1])))
for z in range(0, len(bp.array_r)):
    for v in range(0, len(bp.array_r[1])):
        dist = linalg.norm(bp.RGBvalues(z, v) - mt)
        if dist < p:
            min = dist
            p = dist
        if dist > q:
            max = dist
            q = dist
        array_Eus[z][v] = dist

print('Minimum unscaled Euclidean distance:', min)
print('Maximum unscaled Euclidean distance:', max)

# Euclidean distance scaled
array_Es = np.zeros((len(bp.array_r), len(bp.array_r[1])))
for b in range(0, len(bp.array_r)):
    for v in range(0, len(bp.array_r[1])):
        dist = linalg.norm(bp.RGBvalues(b, v) - mt)
        dist = ((dist - min) * 255) / (max - min)  # Linear scaling to 0-255
        dist = float("{0:.2f}".format(dist))   # Round to two decimal places
        array_Es[b][v] = dist

# save array as image
Bild_M = Image.fromarray(array_Es)
Bild_M = Bild_M.convert("L")  # grayscale image
Bild_M.save("../outputs/" + bp.filename + "_source1.jpg")

# Histogram
# Unscaled Euclidean distance of the training set
p = 255
q = 0
array_ETus = np.zeros(len(bp.array_Tr))
for r in range(0, len(bp.array_Tr)):
    dist = linalg.norm(bp.RGBvaluesTrainingset(r) - mt)
    if dist < p:
        min = dist
        p = dist
    if dist > q:
        max = dist
        q = dist
    array_ETus[r] = dist

# Scaled Euclidean distance of the training set
array_ETs = np.zeros(len(bp.array_Tr))
for p in range(0, len(bp.array_Tr)):
    dist = linalg.norm(bp.RGBvaluesTrainingset(p) - mt)
    dist = ((dist - min) * 255) / (max - min)
    dist = float("{0:.2f}".format(dist))
    array_ETs[p] = dist

# Euclidean distance from individual areas
array_stones = np.zeros(290 * 288)
array_water = np.zeros(287 * 288)
array_sand = np.zeros(290 * 287)
array_wood = np.zeros(287 * 287)
i = 0
for t in range(0, 288):
    for s in range(0,290):
        array_stones[i] = array_Es[t][s]
        i = i + 1
i = 0
for t in range(0, 288):
    for s in range(290,577):
        array_water[i] = array_Es[t][s]
        i = i + 1
i = 0
for t in range(288, 575):
    for s in range(0,290):
        array_sand[i] = array_Es[t][s]
        i = i + 1
i = 0
for t in range(288, 575):
    for s in range(290,577):
        array_wood[i] = array_Es[t][s]
        i = i + 1

# Create histogram
plt.hist(np.asarray(array_stones), alpha=0.3, color="green", density=True, label="pixels in the stone area")
plt.hist(np.asarray(array_water), alpha=0.3, color="yellow", density=True, label="pixels in the water area")
plt.hist(np.asarray(array_sand), alpha=0.3, color="orange", density=True, label="pixels in the sand area")
plt.hist(np.asarray(array_wood), alpha=0.3, color="blue", density=True, label="pixels in the wood area")
plt.hist(np.asarray(array_ETs), alpha=0.3, color="red", density=True, label="pixels from the training set")
plt.title(bp.filename + ".jpg")
plt.xlabel('Euclidean distance', fontsize=13)
plt.ylabel('Frequency', fontsize=13)
plt.legend(loc='upper right')
plt.savefig("../outputs/" + bp.filename + "_histogram-source1.png")
plt.show()

end = time.time()
print("Duration of Source 1 (without DST):",'{:5.3f}s'.format(end-start))

# Dempster-Shafer theory
start = time.time()

# complements of the normalized euclidean distances
o = len(array_Eus) * len(array_Eus[1])
array_Eus1 = array_Eus.reshape((o, ))
mi = 100000000
ma = 0
for i in range(0,len(array_Eus1)):
    if mi > array_Eus1[i]:
        mi = array_Eus1[i]
    if ma < array_Eus1[i]:
        ma = array_Eus1[i]

a = np.ones(len(array_Eus1))
array_Eus1 = a - np.divide(array_Eus1 - mi * a, (ma - mi) * a)

# Set values of the mass function
theta = array_Eus1.std()                     # Let theta be omega\cup omegak
x = a - theta
omega = np.multiply(array_Eus1, x)
omega = omega.round(4)                       # Omega: Pixel belongs to the image area being examined
omegak = x - omega
omegak = omegak.round(4)                     # Omega: Pixel belongs to the image area that is not examined
theta = theta.round(4)                       # Uncertainty

print("m({\omega}) for all pixels:", omega)
print("m({\overline\omega}) for all pixels:", omegak)
print("m({\omega,\omegak}) for all pixels:", theta)

# Set values of the belief function
Belomega = omega
Belomegak = omegak
Beltheta = omega + omegak + theta

print("Bel({\omega}) for all pixels:", Belomega)
print("Bel({\overline\omega}) for all pixels:", Belomegak)
print("Bel({\omega,\omegak}) for all pixels:", Beltheta)

# Set values of the belief function
Plomega = omega + theta
Plomegak = omegak + theta
Pltheta = omega + omegak + theta

print("Pl({\omega}) for all pixels:", Plomega)
print("Pl({\overline\omega}) for all pixels:", Plomegak)
print("Pl({\omega,\omegak}) for all pixels:", Pltheta)

# Three different decision rules
Dempster = np.zeros(len(Belomega))

# weakest decision rule
for k in range(0, len(Belomega)-1):
    if Plomega[k] > Belomegak[k]:
        Dempster[k] = 100

# average decision rule
for k in range(0, len(Belomega)-1):
    if Belomega[k] > Belomegak[k]:
        Dempster[k] = 220

# strongest decision rule
for k in range(0, len(Belomega)-1):
    if Belomega[k] > Plomegak[k]:
        Dempster[k] = 255

Dempster = Dempster.reshape((len(array_Eus), len(array_Eus[1])))
# save array as image
Image_M = Image.fromarray(Dempster)
Image_M = Image_M.convert("L")  # grayscale image
Image_M.save("../outputs/" + bp.filename + "_DST-source1.jpg")

end = time.time()
print("Duration of DST:",'{:5.3f}s'.format(end-start))