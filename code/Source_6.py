import numpy as np
import basic_program as bp
import time

# Source 6: expert opinion
print("Source 6:")
# Note that this source is adapted to the sample image of my thesis and cannot be applied to any image!!!

# Dempster-Shafer theory
start = time.time()

# Set values of the mass function
o = 575 * 577
theta = np.ones(575*577)
for j in range (0, 288):
    for i in range(0, 290):
        theta[i + j * 577] = 0
omega = np.zeros(575 * 577)
omegak = np.zeros(575 * 577)
for j in range (0, 288):
    for i in range(0, 290):
        omegak[i + j * 577] = 1

print("m({\omega}) for all pixels:", omega)
print("m({\overline\omega}) for all pixels:", omegak)
print("m({\omega,\omegak}) for all pixels:", theta)

# Set values of the belief function
Belomega = omega
Belomegak = omegak
Beltheta = omega + omegak + theta

print("Bel({\omega}) for all pixels:", Belomega)
print("Bel({\overline\omega}) for all pixels:", Belomegak)
print("Bel({\omega,\omegak}) for all pixels:", Beltheta)

# Set values of the plausibility function
Plomega = omega + theta
Plomegak = omegak + theta
Pltheta = omega + omegak + theta

print("Pl({\omega}) for all pixels:", Plomega)
print("Pl({\overline\omega}) for all pixels:", Plomegak)
print("Pl({\omega,\omegak}) for all pixels:", Pltheta)

# Three different decision rules
Dempster = np.zeros(len(Belomega))

# weakest decision rule
for k in range(0, len(Belomega)-1):
    if Plomega[k] > Belomegak[k]:
        Dempster[k] = 100

# average decision rule
for k in range(0, len(Belomega)-1):
    if Belomega[k] > Belomegak[k]:
        Dempster[k] = 220

# strongest decision rule
for k in range(0, len(Belomega)-1):
    if Belomega[k] > Plomegak[k]:
        Dempster[k] = 255

Dempster = Dempster.reshape((575,577))
# save array as image
Image_M = bp.Image.fromarray(Dempster)
Image_M = Image_M.convert("L")  # grayscale image
Image_M.save("../outputs/" + bp.filename + "_DST-source6.jpg")

end = time.time()
print("Duration of DST:",'{:5.3f}s'.format(end-start))

o = len(bp.array_r) * len(bp.array_r[1])
array_r2 = bp.array_r.reshape((o, ))
a = np.ones(len(array_r2))