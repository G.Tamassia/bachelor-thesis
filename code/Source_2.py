import basic_program as bp
import numpy as np
import math
import time
import matplotlib.pyplot as plt
import numpy.linalg as linalg
from numpy.linalg import inv
from PIL import Image
start = time.time()

# Source 2: Segmentation using the RGB values of a pixel using the Mahalanobis distance in R^3
print("Source 2:")

# No manual entries

# Unbiased sample mean of the training set (3-dim)
mt = np.array((0, 0, 0))
for i in range(0, len(bp.array_Tr)):
    mt = mt + bp.RGBvaluesTrainingset(i)

mt = mt / len(bp.array_Tr)
print("Unbiased sample mean of the training set:")
print(mt)

# Determine covariance matrix
Vec = []
for i in range(0, len(bp.array_Tr)):
    Vec.append(bp.RGBvaluesTrainingset(i))

Vec = np.array(Vec)
G = np.cov(Vec.T)
print("Covariance matrix of the training set (3x3):")
print(G)
print('Condition of the covariance matrix:',linalg.cond(G))
Kti = inv(G)
print("Inverse covariance matrix of the training set (3x3):")
print(Kti)

M = 0
def MahalanobisDistance(x):   # x must be a vector with three entries [r g b]
    q1 = (mt - x)
    M = q1 * Kti * q1.T
    M = M[0][0]
    M = math.sqrt(M)
    return M

# Mahalanobis distance unscaled
p = 10000000000
q = 0
array_Mus = np.zeros((len(bp.array_r), len(bp.array_r[1])))
for i in range(0, len(bp.array_r)):
    for j in range(0, len(bp.array_r[1])):
        x = bp.RGBvalues(i, j)
        w = MahalanobisDistance(x)
        if w < p:
            min = w
            p = w
        if w > q:
            max = w
            q = w
        array_Mus[i][j] = w

print('Minimum unscaled Mahalanobis distance:', min)
print('Maximum unscaled Mahalanobis distance:', max)


# Mahalanobis distance scaled
array_Ms = np.zeros((len(bp.array_r), len(bp.array_r[1])))
for i in range(0, len(bp.array_r)):
    for j in range(0, len(bp.array_r[1])):
        x = bp.RGBvalues(i, j)
        p = ((MahalanobisDistance(x) - min) * 255) / (max - min)  # Linear scaling to 0-255
        p = float("{0:.2f}".format(p))
        array_Ms[i][j] = p

# save array as image
Bild_M = Image.fromarray(array_Ms)
Bild_M = Bild_M.convert("L")  # grayscale image
Bild_M.save("../outputs/" + bp.filename + "_source2.jpg")

# Histogram
# Unscaled Mahalanobis distance of the training set
p = 255
q = 0
array_MTus = np.zeros(len(bp.array_Tr))
for i in range(0, len(bp.array_Tr)):
    x = bp.RGBvaluesTrainingset(i)
    w = MahalanobisDistance(x)
    if w < p:
        min1 = w
        p = w
    if w > q:
        max1 = w
        q = w
    array_MTus[i] = w

# Scaled Euclidean distance of the training set
array_MTs = np.zeros(len(bp.array_Tr))
for i in range(0, len(bp.array_Tr)):
    x = bp.RGBvaluesTrainingset(i)
    w = ((MahalanobisDistance(x) - min1) * 255) / (max1 - min1)
    w = float("{0:.2f}".format(w))
    array_MTs[i] = w

# Mahalanobis distance from individual areas
array_stones1 = np.zeros(290 * 288)
array_water1 = np.zeros(287 * 288)
array_sand1 = np.zeros(290 * 287)
array_wood1 = np.zeros(287 * 287)
i = 0
for t in range(0, 288):
    for s in range(0,290):
        array_stones1[i] = array_Ms[t][s]
        i = i + 1
i = 0
for t in range(0, 288):
    for s in range(290,577):
        array_water1[i] = array_Ms[t][s]
        i = i + 1
i = 0
for t in range(288, 575):
    for s in range(0,290):
        array_sand1[i] = array_Ms[t][s]
        i = i + 1
i = 0
for t in range(288, 575):
    for s in range(290,577):
        array_wood1[i] = array_Ms[t][s]
        i = i + 1

# Create histogram
plt.hist(np.asarray(array_stones1), alpha=0.3, color="green", density=True, label="pixels in the stone area")
plt.hist(np.asarray(array_water1), alpha=0.3, color="yellow", density=True, label="pixels in the water area")
plt.hist(np.asarray(array_sand1), alpha=0.3, color="orange", density=True, label="pixels in the sand area")
plt.hist(np.asarray(array_wood1), alpha=0.3, color="blue", density=True, label="pixels in the wood area")
plt.hist(np.asarray(array_MTs), alpha=0.3, color="red", density=True, label="pixels from the training set")
plt.title(bp.filename + ".jpg")
plt.xlabel('Mahalanobis distance', fontsize=13)
plt.ylabel('Frequency', fontsize=13)
plt.legend(loc='upper right')
plt.savefig("../outputs/" + bp.filename + "_histogram-source2.png")
plt.show()

end = time.time()
print("Duration of Source 2 (without DST):",'{:5.3f}s'.format(end-start))

# Dempster-Shafer theory
start = time.time()

# complements of the normalized mahalanobis distance
o = len(array_Mus) * len(array_Mus[1])
array_Mus1 = array_Mus.reshape((o, ))
mi = 100000000
ma = 0
for i in range(0,len(array_Mus1)):
    if mi > array_Mus1[i]:
        mi = array_Mus1[i]
    if ma < array_Mus1[i]:
        ma = array_Mus1[i]

a = np.ones(len(array_Mus1))
array_Mus1 = a - np.divide(array_Mus1 - mi * a, (ma - mi) * a)

# Set values of the mass function
theta = array_Mus1.std()                     # Let theta be omega\cup omegak
x = a - theta
omega = np.multiply(array_Mus1, x)
omega = omega.round(4)                       # Omega: Pixel belongs to the image area being examined
omegak = x - omega
omegak = omegak.round(4)                     # Omega: Pixel belongs to the image area that is not examined
theta = theta.round(4)                       # Uncertainty

print("m({\omega}) for all pixels:", omega)
print("m({\overline\omega}) for all pixels:", omegak)
print("m({\omega,\omegak}) for all pixels:", theta)

# Set values of the belief function
Belomega = omega
Belomegak = omegak
Beltheta = omega + omegak + theta

print("Bel({\omega}) for all pixels:", Belomega)
print("Bel({\overline\omega}) for all pixels:", Belomegak)
print("Bel({\omega,\omegak}) for all pixels:", Beltheta)

# Set values of the belief function
Plomega = omega + theta
Plomegak = omegak + theta
Pltheta = omega + omegak + theta

print("Pl({\omega}) for all pixels:", Plomega)
print("Pl({\overline\omega}) for all pixels:", Plomegak)
print("Pl({\omega,\omegak}) for all pixels:", Pltheta)

# Three different decision rules
Dempster = np.zeros(len(Belomega))

# weakest decision rule
for k in range(0, len(Belomega)-1):
    if Plomega[k] > Belomegak[k]:
        Dempster[k] = 100

# average decision rule
for k in range(0, len(Belomega)-1):
    if Belomega[k] > Belomegak[k]:
        Dempster[k] = 220

# strongest decision rule
for k in range(0, len(Belomega)-1):
    if Belomega[k] > Plomegak[k]:
        Dempster[k] = 255

Dempster = Dempster.reshape((len(array_Mus), len(array_Mus[1])))
# save array as image
Image_M = Image.fromarray(Dempster)
Image_M = Image_M.convert("L")  # grayscale image
Image_M.save("../outputs/" + bp.filename + "_DST-source2.jpg")

end = time.time()
print("Duration of DST:",'{:5.3f}s'.format(end-start))