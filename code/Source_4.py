import basic_program as bp
import numpy as np
import math
import time
import numpy.linalg as linalg
from numpy.linalg import inv
from PIL import Image
start = time.time()


# Source 4: Segmentation based on the surrounding texture of a pixel in the form of a hyperbolic cross in R^{3(g+(g-1)+4k!)x1}
# using the Mahalanobis distance
print("Source 4:")

# Manual entries
# Enter here the size of the texture to be considered (g should be odd to have a center)
g = 5
# Specify the size of the corners at the crosses (it must k < int(g/2-0. 5) apply)
k = 1
# Enter a small eps to improve the condition
eps = 0.1


# Define multidimensional training set pixel with the texture of a hyperbolic cross
def MultidimVectorTrain(g,k):
    array_MdVT = np.ones((len(bp.array_Tr), int((1 + 4 * (g / 2 -0.5) + 2 * k * (k + 1)) * 3)))
    e = -int(g / 2 - 0.5)
    f = int(g / 2 - 0.5) + 1
    for i in range(0, len(bp.array_Tr)):
        t = 0
        for p in range(e, f):
            if (bp.Pixelcoord[i][0] + p) > -1 and (bp.Pixelcoord[i][0] + p) < len(bp.array_r):
                array_MdVT[i][t] = bp.array_r[bp.Pixelcoord[i][0] + p][bp.Pixelcoord[i][1]]
                array_MdVT[i][t + 1] = bp.array_g[bp.Pixelcoord[i][0] + p][bp.Pixelcoord[i][1]]
                array_MdVT[i][t + 2] = bp.array_b[bp.Pixelcoord[i][0] + p][bp.Pixelcoord[i][1]]
                t = t + 3
            else:
                array_MdVT[i][t] = bp.array_r[bp.Pixelcoord[i][0] - p][bp.Pixelcoord[i][1]]
                array_MdVT[i][t + 1] = bp.array_g[bp.Pixelcoord[i][0] - p][bp.Pixelcoord[i][1]]
                array_MdVT[i][t + 2] = bp.array_b[bp.Pixelcoord[i][0] - p][bp.Pixelcoord[i][1]]
                t = t + 3
        for q in range(e, 0):
            if (bp.Pixelcoord[i][1] + q) > -1 and (bp.Pixelcoord[i][1] + q) < len(bp.array_r[1]):
                array_MdVT[i][t] = bp.array_r[bp.Pixelcoord[i][0]][bp.Pixelcoord[i][1] + q]
                array_MdVT[i][t + 1] = bp.array_g[bp.Pixelcoord[i][0]][bp.Pixelcoord[i][1] + q]
                array_MdVT[i][t + 2] = bp.array_b[bp.Pixelcoord[i][0]][bp.Pixelcoord[i][1] + q]
                t = t + 3
            else:
                array_MdVT[i][t] = bp.array_r[bp.Pixelcoord[i][0]][bp.Pixelcoord[i][1] - q]
                array_MdVT[i][t + 1] = bp.array_g[bp.Pixelcoord[i][0]][bp.Pixelcoord[i][1] - q]
                array_MdVT[i][t + 2] = bp.array_b[bp.Pixelcoord[i][0]][bp.Pixelcoord[i][1] - q]
                t = t + 3
        for q in range(1, f):
            if (bp.Pixelcoord[i][1] + q) > -1 and (bp.Pixelcoord[i][1] + q) < len(bp.array_r[1]):
                array_MdVT[i][t] = bp.array_r[bp.Pixelcoord[i][0]][bp.Pixelcoord[i][1] + q]
                array_MdVT[i][t + 1] = bp.array_g[bp.Pixelcoord[i][0]][bp.Pixelcoord[i][1] + q]
                array_MdVT[i][t + 2] = bp.array_b[bp.Pixelcoord[i][0]][bp.Pixelcoord[i][1] + q]
                t = t + 3
            else:
                array_MdVT[i][t] = bp.array_r[bp.Pixelcoord[i][0]][bp.Pixelcoord[i][1] - q]
                array_MdVT[i][t + 1] = bp.array_g[bp.Pixelcoord[i][0]][bp.Pixelcoord[i][1] - q]
                array_MdVT[i][t + 2] = bp.array_b[bp.Pixelcoord[i][0]][bp.Pixelcoord[i][1] - q]
                t = t + 3
        z = k + 2
        for q in range(1, k + 1):      # right lower corner of the cross
            z = z - 1
            if z > 1:
                for p in range(1, z):
                    if (bp.Pixelcoord[i][0] + p) > -1 and (bp.Pixelcoord[i][0] + p) < len(bp.array_r) and (bp.Pixelcoord[i][1] + q) > -1 and (bp.Pixelcoord[i][1] + q) < len(bp.array_r[1]):
                        array_MdVT[i][t] = bp.array_r[bp.Pixelcoord[i][0] + p][bp.Pixelcoord[i][1] + q]
                        array_MdVT[i][t + 1] = bp.array_g[bp.Pixelcoord[i][0] + p][bp.Pixelcoord[i][1] + q]
                        array_MdVT[i][t + 2] = bp.array_b[bp.Pixelcoord[i][0] + p][bp.Pixelcoord[i][1] + q]
                        t = t + 3
                    else:
                        array_MdVT[i][t] = 0
                        array_MdVT[i][t + 1] = 0
                        array_MdVT[i][t + 2] = 0
                        t = t + 3
        z = k + 2
        for q in range(1, k + 1):      # right upper corner of the cross
            z = z - 1
            if z > 1:
                for p in range(1, z):
                    if (bp.Pixelcoord[i][0] - p) > -1 and (bp.Pixelcoord[i][0] - p) < len(bp.array_r) and (
                            bp.Pixelcoord[i][1] + q) > -1 and (bp.Pixelcoord[i][1] + q) < len(bp.array_r[1]):
                        array_MdVT[i][t] = bp.array_r[bp.Pixelcoord[i][0] - p][bp.Pixelcoord[i][1] + q]
                        array_MdVT[i][t + 1] = bp.array_g[bp.Pixelcoord[i][0] - p][bp.Pixelcoord[i][1] + q]
                        array_MdVT[i][t + 2] = bp.array_b[bp.Pixelcoord[i][0] - p][bp.Pixelcoord[i][1] + q]
                        t = t + 3
                    else:
                        array_MdVT[i][t] = 0
                        array_MdVT[i][t + 1] = 0
                        array_MdVT[i][t + 2] = 0
                        t = t + 3
        z = k + 2
        for q in range(1, k + 1):       # left bottom corner of the cross
            z = z - 1
            if z > 1:
                for p in range(1, z):
                    if (bp.Pixelcoord[i][0] + p) > -1 and (bp.Pixelcoord[i][0] + p) < len(bp.array_r) and (
                            bp.Pixelcoord[i][1] - q) > -1 and (bp.Pixelcoord[i][1] - q) < len(bp.array_r[1]):
                        array_MdVT[i][t] = bp.array_r[bp.Pixelcoord[i][0] + p][bp.Pixelcoord[i][1] - q]
                        array_MdVT[i][t + 1] = bp.array_g[bp.Pixelcoord[i][0] + p][bp.Pixelcoord[i][1] - q]
                        array_MdVT[i][t + 2] = bp.array_b[bp.Pixelcoord[i][0] + p][bp.Pixelcoord[i][1] - q]
                        t = t + 3
                    else:
                        array_MdVT[i][t] = 0
                        array_MdVT[i][t + 1] = 0
                        array_MdVT[i][t + 2] = 0
                        t = t + 3
        z = k + 2
        for q in range(1, k + 1):       # left upper corner of the cross
            z = z - 1
            if z > 1:
                for p in range(1, z):
                    if (bp.Pixelcoord[i][0] - p) > -1 and (bp.Pixelcoord[i][0] - p) < len(bp.array_r) and (
                            bp.Pixelcoord[i][1] - q) > -1 and (bp.Pixelcoord[i][1] - q) < len(bp.array_r[1]):
                        array_MdVT[i][t] = bp.array_r[bp.Pixelcoord[i][0] - p][bp.Pixelcoord[i][1] - q]
                        array_MdVT[i][t + 1] = bp.array_g[bp.Pixelcoord[i][0] - p][bp.Pixelcoord[i][1] - q]
                        array_MdVT[i][t + 2] = bp.array_b[bp.Pixelcoord[i][0] - p][bp.Pixelcoord[i][1] - q]
                        t = t + 3
                    else:
                        array_MdVT[i][t] = 0
                        array_MdVT[i][t + 1] = 0
                        array_MdVT[i][t + 2] = 0
                        t = t + 3
    return array_MdVT


w = MultidimVectorTrain(g, k)
Kt2 = np.cov(w.T)
print('Covariance matrix of the trainings set:')
print(Kt2)
print('Condition of the covariance matrix without eps:',linalg.cond(Kt2))
I = np.eye(len(Kt2))
I = np.array(I)
Kt2 = Kt2 + eps * I
print('Condition of the covariance matrix with eps:',linalg.cond(Kt2))
X = inv(Kt2)
X = np.array(X)
print('Inverse Covariance matrix of the trainings set:')
print(X)

# Unbiased sample mean of the trainings set:
def MeanVecMultidimTrain(g, k):
    array_Sum = 0
    array_MdVT = MultidimVectorTrain(g, k)
    array_MdVT = np.asarray(array_MdVT)
    for i in range(0, len(bp.array_Tr)):
        array_Sum = array_Sum + array_MdVT[i]
    array_Mi = array_Sum / len(bp.array_Tr)
    return (array_Mi)

array_Mi = MeanVecMultidimTrain(g, k)
print('Unbiased sample mean of the trainings set:')
print(array_Mi)

# Define multidimensional vectors
def MultidimVectors(g, k):
    array_MdV = np.ones(shape=(len(bp.array_r), len(bp.array_r[0]), int((1 + 4 * (g / 2 -0.5) + 2 * k * (k + 1)) * 3)))
    array_Mus = np.ones((len(bp.array_r), len(bp.array_r[0])))
    e = -int(g / 2 - 0.5)
    f = int(g / 2 - 0.5) + 1
    for i in range(0, len(bp.array_r)):
        for j in range(0, len(bp.array_r[0])):
            t = 0
            for p in range(e, f):
                if (i + p) > -1 and (i + p) < len(bp.array_r):
                    array_MdV[i][j][t] = bp.array_r[i + p][j]
                    array_MdV[i][j][t + 1] = bp.array_g[i + p][j]
                    array_MdV[i][j][t + 2] = bp.array_b[i + p][j]
                    t = t + 3
                else:
                    array_MdV[i][j][t] = bp.array_r[i - p][j]
                    array_MdV[i][j][t + 1] = bp.array_g[i - p][j]
                    array_MdV[i][j][t + 2] = bp.array_b[i - p][j]
                    t = t + 3
            for q in range(e, 0):
                if (j + q) > -1 and (j + q) < len(bp.array_r[0]):
                    array_MdV[i][j][t] = bp.array_r[i][j + q]
                    array_MdV[i][j][t + 1] = bp.array_g[i][j + q]
                    array_MdV[i][j][t + 2] = bp.array_b[i][j + q]
                    t = t + 3
                else:
                    array_MdV[i][j][t] = bp.array_r[i][j - q]
                    array_MdV[i][j][t + 1] = bp.array_g[i][j - q]
                    array_MdV[i][j][t + 2] = bp.array_b[i][j - q]
                    t = t + 3
            for q in range(1, f):
                if (j + q) > -1 and (j + q) < len(bp.array_r[0]):
                    array_MdV[i][j][t] = bp.array_r[i][j + q]
                    array_MdV[i][j][t + 1] = bp.array_g[i][j + q]
                    array_MdV[i][j][t + 2] = bp.array_b[i][j + q]
                    t = t + 3
                else:
                    array_MdV[i][j][t] = bp.array_r[i][j - q]
                    array_MdV[i][j][t + 1] = bp.array_g[i][j - q]
                    array_MdV[i][j][t + 2] = bp.array_b[i][j - q]
                    t = t + 3
            z = k + 2
            for q in range(1, k + 1):      # right lower corner of the cross
                z = z - 1
                if z > 1:
                    for p in range(1, z):
                        if (i + p) > -1 and (i + p) < len(bp.array_r) and (j + q) > -1 and (j + q) < len(bp.array_r[1]):
                            array_MdV[i][j][t] = bp.array_r[i + p][j + q]
                            array_MdV[i][j][t + 1] = bp.array_g[i + p][j + q]
                            array_MdV[i][j][t + 2] = bp.array_b[i + p][j + q]
                            t = t + 3
                        else:
                            array_MdV[i][j][t] = 0
                            array_MdV[i][j][t + 1] = 0
                            array_MdV[i][j][t + 2] = 0
                            t = t + 3
            z = k + 2
            for q in range(1, k + 1):      # right upper corner of the cross
                z = z - 1
                if z > 1:
                    for p in range(1, z):
                        if (i - p) > -1 and (i - p) < len(bp.array_r) and (j + q) > -1 and (j + q) < len(bp.array_r[1]):
                            array_MdV[i][j][t] = bp.array_r[i - p][j + q]
                            array_MdV[i][j][t + 1] = bp.array_g[i - p][j + q]
                            array_MdV[i][j][t + 2] = bp.array_b[i - p][j + q]
                            t = t + 3
                        else:
                            array_MdV[i][j][t] = 0
                            array_MdV[i][j][t + 1] = 0
                            array_MdV[i][j][t + 2] = 0
                            t = t + 3
            z = k + 2
            for q in range(1, k + 1):      # left lower corner of the cross
                z = z - 1
                if z > 1:
                    for p in range(1, z):
                        if (i + p) > -1 and (i + p) < len(bp.array_r) and (j - q) > -1 and (j - q) < len(bp.array_r[1]):
                            array_MdV[i][j][t] = bp.array_r[i + p][j - q]
                            array_MdV[i][j][t + 1] = bp.array_g[i + p][j - q]
                            array_MdV[i][j][t + 2] = bp.array_b[i + p][j - q]
                            t = t + 3
                        else:
                            array_MdV[i][j][t] = 0
                            array_MdV[i][j][t + 1] = 0
                            array_MdV[i][j][t + 2] = 0
                            t = t + 3
            z = k + 2
            for q in range(1, k + 1):       # left upper corner of the cross
                z = z - 1
                if z > 1:
                    for p in range(1, z):
                        if (i - p) > -1 and (i - p) < len(bp.array_r) and (j - q) > -1 and (j - q) < len(bp.array_r[1]):
                            array_MdV[i][j][t] = bp.array_r[i - p][j - q]
                            array_MdV[i][j][t + 1] = bp.array_g[i - p][j - q]
                            array_MdV[i][j][t + 2] = bp.array_b[i - p][j - q]
                            t = t + 3
                        else:
                            array_MdV[i][j][t] = 0
                            array_MdV[i][j][t + 1] = 0
                            array_MdV[i][j][t + 2] = 0
                            t = t + 3
            q = array_Mi - array_MdV[i][j]
            q = np.asmatrix(q)
            MM = q * X * q.T
            MM = MM[0][0]
            MM = math.sqrt(MM)
            array_Mus[i][j] = MM
    return array_MdV, array_Mus


array_MdV, array_Mus = MultidimVectors(g, k)
min = np.amin(array_Mus)
max = np.amax(array_Mus)

print('Minimum unscaled Mahalanobis distance:', min)
print('Maximum unscaled Mahalanobis distance:', max)

# Scale the Mahalanobis distance
array_Mah = ((array_Mus - min) * 255) / (max - min)

# Save array as image
Image_M = Image.fromarray(array_Mah)
Image_M = Image_M.convert("L")  # grayscale image
Image_M.save("../outputs/" + bp.filename + "_source4.jpg")

end = time.time()
print("Duration of Source 4 (without DST):",'{:5.3f}s'.format(end-start))


# Dempster-Shafer theory
start = time.time()

# complements of the normalized mahalanobis distance
o = len(array_Mus) * len(array_Mus[1])
array_Mus1 = array_Mus.reshape((o, ))
mi = 100000000
ma = 0
for i in range(0,len(array_Mus1)):
    if mi > array_Mus1[i]:
        mi = array_Mus1[i]
    if ma < array_Mus1[i]:
        ma = array_Mus1[i]

a = np.ones(len(array_Mus1))
array_Mus1 = a - np.divide(array_Mus1 - mi * a, (ma - mi) * a)

# Set values of the mass function
theta = array_Mus1.std()                     # Let theta be omega\cup omegak
x = a - theta
omega = np.multiply(array_Mus1, x)
omega = omega.round(4)                       # Omega: Pixel belongs to the image area being examined
omegak = x - omega
omegak = omegak.round(4)                     # Omega: Pixel belongs to the image area that is not examined
theta = theta.round(4)                       # Uncertainty

print("m({\omega}) for all pixels:", omega)
print("m({\overline\omega}) for all pixels:", omegak)
print("m({\omega,\omegak}) for all pixels:", theta)

# Set values of the belief function
Belomega = omega
Belomegak = omegak
Beltheta = omega + omegak + theta

print("Bel({\omega}) for all pixels:", Belomega)
print("Bel({\overline\omega}) for all pixels:", Belomegak)
print("Bel({\omega,\omegak}) for all pixels:", Beltheta)

# Set values of the belief function
Plomega = omega + theta
Plomegak = omegak + theta
Pltheta = omega + omegak + theta

print("Pl({\omega}) for all pixels:", Plomega)
print("Pl({\overline\omega}) for all pixels:", Plomegak)
print("Pl({\omega,\omegak}) for all pixels:", Pltheta)

# Three different decision rules
Dempster = np.zeros(len(Belomega))

# weakest decision rule
for k in range(0, len(Belomega)-1):
    if Plomega[k] > Belomegak[k]:
        Dempster[k] = 100

# average decision rule
for k in range(0, len(Belomega)-1):
    if Belomega[k] > Belomegak[k]:
        Dempster[k] = 220

# strongest decision rule
for k in range(0, len(Belomega)-1):
    if Belomega[k] > Plomegak[k]:
        Dempster[k] = 255

Dempster = Dempster.reshape((len(array_Mus), len(array_Mus[1])))
# save array as image
Image_M = Image.fromarray(Dempster)
Image_M = Image_M.convert("L")  # grayscale image
Image_M.save("../outputs/" + bp.filename + "_DST-source4.jpg")

end = time.time()
print("Duration of DST:",'{:5.3f}s'.format(end-start))