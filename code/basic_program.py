import numpy as np
import os
import time
from PIL import Image
from turtle import Turtle, Screen
start = time.time()

# Manual entries
# Enter the name of the picture here:
Name = "../figs/NaturalMaterials.jpg"

# Convert image to jpg/RGB image
def Convert_RGBimage(img_name):
    infile = img_name
    image = Image.open(img_name)
    if not infile.endswith(".jpg"):
        filename, extension = os.path.splitext(os.path.basename(infile))
        outfile = filename + ".jpg"
        rgbImage = image.convert("RGB")
        rgbImage.save("../outputs/" + outfile)
        Name = os.path.basename("../outputs/" + outfile)
        return outfile, Name
    else:
        infile = os.path.basename(infile)
        rgbImage = image.convert("RGB")
        rgbImage.save("../outputs/" + infile)
        Name = os.path.basename("../outputs/" + infile)
        return infile, Name
    image.close()


x, Name = Convert_RGBimage(Name)

# Separate image into individual channels
filename, extension = os.path.splitext(Name)

# Split image into 3 color images (r,g,b)
def single_colour_pictures(img_name):
    image = Image.open("../outputs/" + img_name)
    r, g, b = image.split()
    r.save("../outputs/" + filename + "_red.jpg")
    g.save("../outputs/" + filename + "_green.jpg")
    b.save("../outputs/" + filename + "_blue.jpg")

single_colour_pictures(Name)

# Open individual images
Colourful_Image = Image.open("../outputs/" + Name)
Image_r = Image.open("../outputs/" + filename + "_red.jpg")
Image_g = Image.open("../outputs/" + filename + "_green.jpg")
Image_b = Image.open("../outputs/" + filename + "_blue.jpg")

# Create array for each image (r,g,b)
array_r = np.array(Image_r)
array_g = np.array(Image_g)
array_b = np.array(Image_b)

# Create a training set
# convert . jpg to . gif (because turtle always needs gif)
outfile = filename + ".gif"
rgbImage = Colourful_Image.convert("RGB")
rgbImage.save("../outputs/" + outfile)

screen = Screen()
screen.title("Training set")
screen.setup(width=len(array_r[1]), height=len(array_r))
screen.bgpic("../outputs/" + outfile)
t = Turtle("turtle")
screen.onscreenclick(t.goto)
t.speed(-1)
t.color("Red")
t.width(3)
# Start position of the turtle (centered)
x0 = 0  # Enter x value of the start position          Middle of the wood: 150
y0 = 0  # Enter y value of the start position          Middle of the wood:-150
t.goto(x0, y0)
t.showturtle()

def Pixel_coordinates(x, y):
    c = int(int(round(len(array_r) / 2)) - y)
    d = int(int(round(len(array_r[1]) / 2)) + x)
    Coor = [c, d]
    return Coor

array_Tr = []
array_Tg = []
array_Tb = []
Pixelcoord = []
u = Pixel_coordinates(x0, y0)
Pixelcoord.append(Pixel_coordinates(x0, y0))
array_Tr.append(array_r[u[0]][u[1]])
array_Tg.append(array_g[u[0]][u[1]])
array_Tb.append(array_b[u[0]][u[1]])

def dragging(x, y):
    t.ondrag(None)
    t.setheading(t.towards(x, y))
    t.goto(x, y)
    PixelTrain = Pixel_coordinates(x, y)
    Pixelcoord.append(Pixel_coordinates(x, y))
    array_Tr.append(array_r[PixelTrain[0]][PixelTrain[1]])  # Create training set arrays (r,g,b)
    array_Tg.append(array_g[PixelTrain[0]][PixelTrain[1]])
    array_Tb.append(array_b[PixelTrain[0]][PixelTrain[1]])
    t.ondrag(dragging)

def clickright():
    t.clear()

def main():
    t.ondrag(dragging)
    screen.mainloop()
    print("RGB values of the training set:")
    print("R-values=",array_Tr)
    print("G-values=",array_Tg)
    print("B-values=",array_Tb)

main()

def RGBvalues(i, j):
    return np.array(((array_r[i][j], array_g[i][j], array_b[i][j])))

def RGBvaluesTrainingset(i):
    return np.array(((array_Tr[i], array_Tg[i], array_Tb[i])))

end = time.time()
print("Duration of the basic program:",'{:5.3f}s'.format(end-start))